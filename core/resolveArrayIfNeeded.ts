
/** @module rxjs-poly */

import { combineLatest, isObservable, Observable, of } from "rxjs";
import { resolveIfNeeded } from "./resolveIfNeeded";
/**
 * Observe deeply an array if needed. Prefer [[resolveArray]].
 * @param source what to observe
 * @return source or deep observable of source if needed
 * @example `resolveArrayIfNeeded([ 1 ])`
 * @example `resolveArrayIfNeeded([ of(1) ])`
 */
export function resolveArrayIfNeeded (
  source: Array<any>
): Observable<Array<any>> | Array<any> {

  if (!source.length) {
    return source;
  }

  let dependent: boolean = false;

  const values: Array<Observable<any>> = source.map(function (
    item: any
  ): Observable<any> {
      const value: any = resolveIfNeeded(item);
      if (isObservable(value)) {
        dependent = true;
        return value;
      }
      return of(item);
    }
  );

  return dependent ? combineLatest(values) : source;
}
