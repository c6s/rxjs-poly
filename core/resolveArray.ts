
/** @module rxjs-poly */

import { Observable } from "rxjs";
import { asObservable } from "./asObservable";
import { resolveArrayIfNeeded } from "./resolveArrayIfNeeded";

/**
 * Observe deeply an array. Prefer [[resolve]].
 * @param source what to observe
 * @return deep observable of source
 * @example `resolveArray([ 1 ])`
 * @example `resolveArray([ of(1) ])`
 */
export function resolveArray ( source: Array<any> ): Observable<Array<any>> {
  return asObservable(resolveArrayIfNeeded(source));
}
