
/** @module rxjs-poly */

import { isObservable, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { resolveArrayIfNeeded } from "./resolveArrayIfNeeded";

/**
 * Observe deeply an object if needed. Prefer [[resolveObject]].
 * @param source what to observe
 * @return source or deep observable of source if needed
 * @example `resolveObjectIfNeeded({ a: 1 })`
 * @example `resolveObjectIfNeeded({ a: of(1) })`
 */
export function resolveObjectIfNeeded (
  source: { [key: string]: any }
): ({ [key: string]: any } | Observable<{ [key: string]: any }>) {

  const keys: Array<string> = Object.keys(source);

  if (!keys.length) {
    return source;
  }

  const values: Array<any> | Observable<Array<any>> =
    resolveArrayIfNeeded(keys.map(
    function ( key: string ): any {
       return source[key];
    }));

  if (!isObservable(values)) {
    return source;
  }

  return values.pipe(map(function (
    list: Array<any>
  ): { [key: string]: any } {
    const result: { [key: string]: any } = { };
    for (const index in list) {
      result[keys[index]] = list[index];
    }
    return result;
  }));
}
