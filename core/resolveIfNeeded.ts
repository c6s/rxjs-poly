
/** @module rxjs-poly */

import { isObservable } from "rxjs";
import { resolveArrayIfNeeded } from "./resolveArrayIfNeeded";
import { resolveObjectIfNeeded } from "./resolveObjectIfNeeded";

/**
 * Observe deeply if needed. Prefer [[resolve]].
 * @param source what to observe
 * @return source or deep observable of source if needed
 * @example `resolveIfNeeded(1)`
 * @example `resolveIfNeeded(of(1))`
 */
export function resolveIfNeeded ( source: any ): any {
  if (source == null || isObservable(source)) {
    return source;
  } else if (source instanceof Array) {
    return resolveArrayIfNeeded(source) as any;
  } else if (source.constructor === Object) {
    return resolveObjectIfNeeded(source as any) as any;
  }
  return source;
}
