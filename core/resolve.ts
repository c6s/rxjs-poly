
/** @module rxjs-poly */

import { Observable } from "rxjs";
import { asObservable } from "./asObservable";
import { resolveIfNeeded } from "./resolveIfNeeded";

/**
 * Observe deeply by observation
 * @param source what to resolve
 * @return deep observable of source
 * @example `resolve({ a: 1, b: of(2), c: [ 3, of(4) ] })`
 */
export function resolve ( source: any ): Observable<any> {
  return asObservable(resolveIfNeeded(source));
}
