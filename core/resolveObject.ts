
/** @module rxjs-poly */

import { Observable } from "rxjs";
import { asObservable } from "./asObservable";
import { resolveObjectIfNeeded } from "./resolveObjectIfNeeded";

/**
 * Observe deeply an object. Prefer [[resolve]].
 * @param source what to observe
 * @return deep observable of source
 * @example `resolveObject({ a: 1 })`
 * @example `resolveObject({ a: of(1) })`
 */
export function resolveObject (
  source: { [key: string]: any }
): Observable<{ [key: string]: any }> {
  return asObservable(resolveObjectIfNeeded(source));
}
