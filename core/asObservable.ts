
/** @module rxjs-poly */

import { isObservable, Observable, of } from "rxjs";

/**
 * Ensure object is observable, wrap if not
 * @param object source
 * @return source or observable of source
 * @example `asObservable(1)`
 * @example `asObservable(of(1))`
 */
export function asObservable<T> ( object: T | Observable<T> ): Observable<T> {
  return isObservable(object) ? object : of<T>(object);
}
