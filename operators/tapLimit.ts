
/** @module rxjs-poly/operators */

import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

/**
 * Receive tap input n times, ignore after
 * @param limit how many times to tap
 * @param callback tap handler
 * @return pipable operator
 * @example `interval(1).pipe(tapLimit(3, console.log.bind(console)));`
 */
export function tapLimit<T> (
  limit: number, callback: ( next: T ) => void
): ( source: Observable<T> ) => Observable<T> {
  return function ( source: Observable<T> ): Observable<T> {
    let count: number = 0;
    return source.pipe(tap(function ( next: T ): void {
      if (count < limit) {
        count += 1;
        callback(next);
      }
    }));
  };
}
