
/** @module rxjs-poly/operators */

import { Observable } from "rxjs";
import { tapLimit } from "./tapLimit";

/**
 * Receive tap input 1 time, ignore after
 * @param callback tap handler
 * @return pipable operator
 * @example `interval(1).pipe(once(console.log.bind(console)));``
 * @see [[tapLimit]]
 */
export function once<T> (
  callback: ( next: T ) => void
): ( source: Observable<T> ) => Observable<T> {
  return tapLimit(1, callback);
}
