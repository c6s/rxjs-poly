
rxjs-poly
===

[![pipeline status](https://gitlab.com/c6s/open/rxjs-poly/badges/master/pipeline.svg)](https://gitlab.com/c6s/open/rxjs-poly/commits/master)
[![coverage report](https://gitlab.com/c6s/open/rxjs-poly/badges/master/coverage.svg)](https://gitlab.com/c6s/open/rxjs-poly/commits/master)

> Polyfill functionality for rxjs, and yes it's strongly typed

None of the functionality here is new in concept, just missing from the rxjs base implementation. There are many implementations of these methods, these were used and modified for their performance as well as clarity of use. If items here are added into rxjs, they will be preferred over ones from this library.

This library exports all rxjs functionality so you may rely on it rather than needing to rely on two import statements, for example:

```ts
import { resolve, combineLatest } from "rxjs-poly";
```

Contribute
---

Please suggest additional features, but understand the goal is to have these added to rxjs or in some cases, the Reactive spec. Please consider the use case for your feature and ensure implementing it will not break any of the principles of the Reactive spec, or damage performance in any way.


Caveat
---

Before you say anything! Yes, the resolve methods are any-typed. This is not without extreme frustration, I spent about 12 hours attempting to strongly type these only to find short-coming after short-coming in the type system for deeply nested conversion. If anyone has a method for typing these that is elegant and maintainable, please let me know - I will continue to look for a better way, but I have to move on with the project and may not be able to return to this for a while. For the time being I recommend you strongly type all input and output of the resolvers.

```ts
import { resolve } from "rxjs-poly";

const input: { a: Array<Observable<number>> } = { a: [ of(1) ] };
const output: { a: Array<number> } = resolve(input);
```

Functionality
---

- - -

### asObservable

> Ensure object is an observable, wrap it not.

```ts
import { asObservable, of } from "rxjs-poly";

asObservable(1);
// Observable<1>

asObservable(of(1));
// Observable<1>
```

Equivalent to

```ts
import { isObservable, of } from "rxjs";

const value: any = 1;
((isObservable(value) ? value : of(value))).subscribe
```

- - -

### resolveArrayIfNeeded

> Internal function: use [resolveArray](#resolveArray).

```ts
import { resolveArrayIfNeeded, of } from "rxjs-poly";

resolveArrayIfNeeded([1, 2, 3]);
// [ 1, 2, 3 ]

resolveArrayIfNeeded([ of(1), 2, 3 ]);
// Observable<[1, 2, 3]>
```

- - -

### resolveArray

> Observe deeply an array.
> Prefer [resolve](#resolve).

```ts
import { resolveArray, of } from "rxjs-poly";

resolveArray([1, 2, 3]);
// Observable<[1, 2, 3]>

resolveArray([of(1), 2, 3]);
// Observable<[1, 2, 3]>
```

Equivalent to

```ts
import { resolveArrayIfNeeded, asObservable } from "rxjs-poly";

const value: Array<any>;
asObservable(resolveArrayIfNeeded(value));
```

- - -

### resolveObjectIfNeeded

> Internal function: use [resolveObject](#resolveObject).

```ts
import { resolveObjectIfNeeded, of } from "rxjs-poly";

resolveObjectIfNeeded({ a: 1 });
// { a: 1 }

resolveObjectIfNeeded({ a: of(1) });
// Observable<{ a: 1 }>
```

- - -

### resolveObject

> Observe deeply an object.
> Prefer [resolve](#resolve).

```ts
import { resolveObject, of } from "rxjs-poly";

resolveObject({ a: 1 });
// Observable<{ a: 1 }>

resolveObject({ a: of(1) });
// Observable<{ a: 1 }>
```

Equivalent to

```ts
import { resolveObjectIfNeeded, asObservable } from "rxjs-poly";

const value: { [key: string]: any };
asObservable(resolveObjectIfNeeded(value));
```

- - -

### resolveIfNeeded

> Internal function: use [resolve](#resolve).

```ts
import { resolveIfNeeded, of } from "rxjs-poly";

resolveIfNeeded(1);
// 1
resolveIfNeeded(of(1));
// Observable<1>

resolveIfNeeded([ 1 ]);
// [ 1 ]
resolveIfNeeded([ of(1) ]);
// Observable<[ 1 ]>

resolveIfNeeded({ a: 1 });
// { a: 1 }
resolveIfNeeded({ a: of(1) });
// Observable<{a: 1}>
```

- - -

### resolve

> Observe deeply any object.

```ts
import { resolve, of } from "rxjs-poly";

resolve(1);
// Observable<1>
resolve(of(1));
// Observable<1>

resolve([ 1 ]);
// Observable<[ 1 ]>
resolve([ of(1) ]);
// Observable<[ 1 ]>

resolve({ a: 1 });
// Observable<{ a: 1 }>
resolve({ a: of(1) });
// Observable<{a: 1}>

// Of note, resolves deeply
resolve({ a: of(1), b: [ of(2) ]})
// Observable<{ a: 1, b: [ 2 ] }>
```

Equivalent to

```ts
import { resolveIfNeeded, asObservable } from "rxjs-poly";

const value: any;
asObservable(resolveIfNeeded(value));
```

- - -

Operators
---

- - -

### tapLimit

> Receive tap input n times, ignore after

```ts
import { interval } from "rxjs-poly";
import { tapLimit } from "rxjs-poly/operators";

interval(1).pipe(tapLimit(3, console.log.bind(console)));
// 1
// 2
// 3
```

- - -

### once

> Receive tap output once, ignore after

```ts
import { interval } from "rxjs-poly";
import { once } from "rxjs-poly/operators";

interval(1).pipe(once(console.log.bind(console)));
// 1
```

Equivalent to

```ts
import { interval } from "rxjs-poly";
import { tapLimit } from "rxjs-poly/operators";

interval(1).pipe(tapLimit(1, console.log.bind(console)));
```

- - -

Want JS?
---

For now, this library is natively written in TypeScript. It will be exposed as a compiled library for JS purists as `rxjs-poly-js` in the future. For the time being, please add a compile phase to your install script:

```json
"scripts": {
  "build:rxjs-poly": "tsc -p node_modules/rxjs-poly",
  "postinstall": "npm run build:rxjs-poly"
},
"devDependencies": {
  "typescript": "latest"
}
```
