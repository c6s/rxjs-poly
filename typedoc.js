
module.exports = {
  mode: "modules",
  exclude: [
    "**/test/**/*",
    "**/index*"
  ],
  module: "commonjs",
  target: "ES5",
  theme: "default"
};
