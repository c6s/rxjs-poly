
import { expect } from "chai";
import { asObservable, isObservable, Observable, of } from "../../";

describe("asObservable", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(asObservable).to.exist.and.be.a("function");
  });
  it("should preserve existing observable", function ( ): void {
    const input: Observable<number> = of(1);
    expect(asObservable(input)).to.equal(input);
  });
  it("should wrap non-observable", function ( ): void {
    const input: number = 1;
    expect(isObservable(asObservable(input))).to.equal(true);
  });
});
