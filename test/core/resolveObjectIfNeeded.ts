
import { expect } from "chai";
import { resolveObjectIfNeeded } from "../../";

describe("resolveObjectIfNeeded", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(resolveObjectIfNeeded).to.exist.and.be.a("function");
  });
  it("should preserve existing empty object", function ( ): void {
    const input: {[key: string]: any} = { };
    expect(resolveObjectIfNeeded(input)).to.equal(input);
  });
  it("should preserve existing plain object", function ( ): void {
    const input: {[key: string]: number} = { a: 1 };
    expect(resolveObjectIfNeeded(input)).to.equal(input);
  });
});
