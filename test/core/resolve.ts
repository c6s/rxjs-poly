
import { expect } from "chai";
import { isObservable, Observable, of, resolve } from "../../";

describe("resolve", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(resolve).to.exist.and.be.a("function");
  });
  it("should preserve existing observable", function ( ): void {
    const input: Observable<number> = of(1);
    expect(resolve(input)).to.equal(input);
  });
  it("should resolve non-observable", function ( ): void {
    const input: number = 1;
    expect(isObservable(resolve(input))).to.equal(true);
  });
  it("should resolve plain array", function ( ): void {
    const input: Array<number> = [ 1 ];
    resolve(input).subscribe(function ( x: Array<number> ): void {
      expect(x).to.equal(input);
    });
  });
  it("should resolve array with observable", function ( ): void {
    const input: Array<Observable<number>> = [ of(1) ];
    resolve(input).subscribe(function ( x: Array<number> ): void {
      expect(x).to.deep.equal([ 1 ]);
    });
  });
  it("should resolve plain object", function ( ): void {
    const input: {[key: string]: number} = { a: 1 };
    resolve(input).subscribe(function ( x: {[key: string]: number} ): void {
      expect(x).to.equal(input);
    });
  });
  it("should resolve object with observable", function ( ): void {
    const input: {[key: string]: Observable<number> } = { a: of(1) };
    resolve(input).subscribe(function ( x: {[key: string]: number} ): void {
      expect(x).to.deep.equal({ a: 1 });
    });
  });
  it("should resolve mixed object", function ( ): void {
    const input: {
      [key: string]: Observable<number> | number
    } = { a: 1, b: of(1) };
    resolve(input).subscribe(function ( x: {[key: string]: number} ): void {
      expect(x).to.deep.equal({ a: 1, b: 1 });
    });
  });
  it("should resolve mixed array", function ( ): void {
    const input: Array<Observable<number> | number> = [ 1, of(1) ];
    resolve(input).subscribe(function ( x: Array<number> ): void {
      expect(x).to.deep.equal([ 1, 1 ]);
    });
  });
  it("should resolve nested object", function ( ): void {
    const input: {
      [key: string]: {
        [key: string]: Observable<number>
      }
    } = { a: { b: of(1) } };
    resolve(input).subscribe(function ( x: {
      [key: string]: {
        [key: string]: number
      }
    } ): void {
      expect(x).to.deep.equal({ a: { b: 1 } });
    });
  });
  it("should resolve nexted array", function ( ): void {
    const input: Array<Array<Observable<number>>> = [ [ of(1) ] ];
    resolve(input).subscribe(function ( x: Array<Array<number>> ): void {
      expect(x).to.deep.equal([ [ 1 ] ]);
    });
  });
  it("should resolve complex object", function ( ): void {
    const input: {
      [key: string]: Array<Observable<number>>
    } = { a: [ of(1) ] };
    resolve(input).subscribe(function (
      x: { [key: string]: Array<number> }
    ): void { expect(x).to.deep.equal({ a: [ 1 ] }); });
  });
  it("should resolve complex array", function ( ): void {
    const input: Array<{
      [key: string]: Observable<number>
    }> = [ { a: of(1) } ];
    resolve(input).subscribe(function (
      x: [ { [key: string]: number } ]
    ): void { expect(x).to.deep.equal([ { a: 1 } ]); });
  });
});
