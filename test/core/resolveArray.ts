
import { expect } from "chai";
import { Observable, of, resolveArray } from "../../";

describe("resolveArray", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(resolveArray).to.exist.and.be.a("function");
  });
  it("should preserve existing plain array", function ( ): void {
    const input: Array<number> = [ 1 ];
    resolveArray(input).subscribe(function ( x: Array<number> ): void {
      expect(x).to.equal(input);
    });
  });
  it("should resolve array of observable", function ( ): void {
    const input: Array<Observable<number>> = [ of(1) ];
    resolveArray(input).subscribe(function ( x: Array<number> ): void {
      expect(x).to.deep.equal([ 1 ]);
    });
  });
  it("should resolve mixed array", function ( ): void {
    const input: Array<number | Observable<number>> = [ 1, of(1) ];
    resolveArray(input).subscribe(function ( x: Array<number> ): void {
      expect(x).to.deep.equal([ 1, 1 ]);
    });
  });
  it("should resolve nested array", function ( ): void {
    const input: Array<Array<Observable<number>>> = [ [ of(1) ] ];
    resolveArray(input).subscribe(function ( x: Array<Array<number>> ): void {
      expect(x).to.deep.equal([ [ 1 ] ]);
    });
  });
});
