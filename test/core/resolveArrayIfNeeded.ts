
import { expect } from "chai";
import { resolveArrayIfNeeded } from "../../";

describe("resolveArrayIfNeeded", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(resolveArrayIfNeeded).to.exist.and.be.a("function");
  });
  it("should preserve existing empty array", function ( ): void {
    const input: Array<any> = [ ];
    expect(resolveArrayIfNeeded(input)).to.equal(input);
  });
  it("should preserve existing plain array", function ( ): void {
    const input: Array<number> = [ 1 ];
    expect(resolveArrayIfNeeded(input)).to.equal(input);
  });
});
