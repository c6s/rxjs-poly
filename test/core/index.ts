
describe("core", function ( ): void {
  require("./asObservable");
  require("./resolve");
  require("./resolveArray");
  require("./resolveArrayIfNeeded");
  require("./resolveObject");
  require("./resolveObjectIfNeeded");
});
