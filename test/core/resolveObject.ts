
import { expect } from "chai";
import { Observable, of, resolveObject } from "../../";

describe("resolveObject", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(resolveObject).to.exist.and.be.a("function");
  });
  it("should preserve existing plain object", function ( ): void {
    const input: {[key: string]: number} = { a: 1 };
    resolveObject(input).subscribe(function (
      x: {[key: string]: number}
    ): void { expect(x).to.equal(input); });
  });
  it("should resolve object with observable", function ( ): void {
    const input: {[key: string]: Observable<number>} = { a: of(1) };
    resolveObject(input).subscribe(function (
      x: {[key: string]: number}
    ): void { expect(x).to.deep.equal({ a: 1 }); });
  });
  it("should resolve mixed object", function ( ): void {
    const input: {
      [key: string]: Observable<number> | number
    } = { a: 1, b: of(1) };
    resolveObject(input).subscribe(function (
      x: {[key: string]: number}
    ): void { expect(x).to.deep.equal({ a: 1, b: 1 }); });
  });
  it("should resolve nested object", function ( ): void {
    const input: {
      [key: string]: {
        [key: string]: Observable<number>
      }
    } = { a: { b: of(1) } };
    resolveObject(input).subscribe(function (
      x: { [key: string]: { [key: string]: number } }
    ): void { expect(x).to.deep.equal({ a: { b: 1 } }); });
  });
});
