
import { expect } from "chai";
import { Observable, of, resolve } from "../../";

describe("resolve", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(resolve).to.exist.and.be.a("function");
  });
  it("should preserve existing observable", function ( ): void {
    const input: Observable<number> = of(1);
    expect(resolve(input)).to.equal(input);
  });
  it("should preserve a plain value", function ( ): void {
    const input: number = 1;
    expect(resolve(input)).to.equal(input);
  });
  it("should preserve a plain array", function ( ): void {
    const input: Array<number> = [ 1 ];
    expect(resolve(input)).to.equal(input);
  });
  it("should preserve a plain object", function ( ): void {
    const input: { [key: string]: number } = { a: 1 };
    expect(resolve(input)).to.equal(input);
  });
  it("should preserve a nested array", function ( ): void {
    const input: Array<Array<number>> = [ [ 1 ] ];
    expect(resolve(input)).to.equal(input);
  });
  it("should preserve a nested object", function ( ): void {
    const input: {
      [key: string]: { [key: string]: number
    } } = { a: { b: 1 } };
    expect(resolve(input)).to.equal(input);
  });
  it("should preserve a complex array", function ( ): void {
    const input: Array<{ [key: string]: number }> = [ { a: 1 } ];
    expect(resolve(input)).to.equal(input);
  });
  it("should preserve a complex object", function ( ): void {
    const input: { [key: string]: Array<number> } = { a: [ 1 ] };
    expect(resolve(input)).to.equal(input);
  });
});
