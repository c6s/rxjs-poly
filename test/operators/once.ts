
import { expect } from "chai";
import Sinon from "sinon";
import { from, noop } from "../../";
import { once } from "../../operators";

describe("once", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(once).to.exist.and.be.a("function");
  });
  it("should wait for input", function ( ): void {
    const spy: Sinon.SinonSpy = Sinon.spy();
    from([ ]).pipe(once(spy)).subscribe(noop);
    expect(spy).to.have.callCount(0);
  });
  it("should accept 1", function ( ): void {
    const spy: Sinon.SinonSpy = Sinon.spy();
    from([1, 2, 3, 4, 5]).pipe(once(spy)).subscribe(noop);
    expect(spy).to.have.callCount(1);
  });
});
