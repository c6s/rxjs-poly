
import { expect } from "chai";
import Sinon from "sinon";
import { from, noop } from "../../";
import { tapLimit } from "../../operators";

describe("tapLimit", function ( ): void {
  it("should exist as a function", function ( ): void {
    expect(tapLimit).to.exist.and.be.a("function");
  });
  it("should accept 0", function ( ): void {
    const spy: Sinon.SinonSpy = Sinon.spy();
    from([1, 2, 3, 4, 5]).pipe(tapLimit(0, spy)).subscribe(noop);
    expect(spy).to.have.callCount(0);
  });
  it("should accept <0", function ( ): void {
    const spy: Sinon.SinonSpy = Sinon.spy();
    from([1, 2, 3, 4, 5]).pipe(tapLimit(-1, spy)).subscribe(noop);
    expect(spy).to.have.callCount(0);
  });
  it("should accept 1", function ( ): void {
    const spy: Sinon.SinonSpy = Sinon.spy();
    from([1, 2, 3, 4, 5]).pipe(tapLimit(1, spy)).subscribe(noop);
    expect(spy).to.have.callCount(1);
  });
  it("should accept >1", function ( ): void {
    const spy: Sinon.SinonSpy = Sinon.spy();
    from([1, 2, 3, 4, 5]).pipe(tapLimit(3, spy)).subscribe(noop);
    expect(spy).to.have.callCount(3);
  });
  it("should accept >l", function ( ): void {
    const spy: Sinon.SinonSpy = Sinon.spy();
    from([1, 2, 3, 4, 5]).pipe(tapLimit(10, spy)).subscribe(noop);
    expect(spy).to.have.callCount(5);
  });
});
