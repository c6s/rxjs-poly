
import * as chai from "chai";
import sinonChai from "sinon-chai";

chai.use(sinonChai);

import "./core";
import "./operators";
